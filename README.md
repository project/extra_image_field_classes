# Extra Image Field Classes

Extra Image Field Classes module will help site administrator to
apply css classes to image fields through "Manage Display" of an
entity type.

Please note "Field UI" module must be enabled to alter your fields.
However, it is not required to display content, so it is not required
to be enabled along with this module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/extra_image_field_classes).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/extra_image_field_classes).


## How it works

1. Go to "Manage Display" of an entity type
2. Select "Extra Image Field Classes" format from Format column for
   image field
3. Click on settings wheel of the image field
4. Enter css classes in Image Class textfield
5. Click "Update" button
6. Remember to click "Save" button after making necessary changes.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have configuration.


## Maintainers

- Rahul Baisane - Original development by [rahulbaisanemca](https://www.drupal.org/u/rahulbaisanemca)
- Dan Feidt - Drupal 9 update by [HongPong](https://www.drupal.org/u/hongpong)
